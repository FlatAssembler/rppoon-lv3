using System;
using System.Collections.Generic;
class MatrixGenerator {
    private static MatrixGenerator instance;
    private RandomGenerator generator;
    private MatrixGenerator()
    {
        this.generator = RandomGenerator.GetInstance();
    }
    public static MatrixGenerator GetInstance()
    {
        if (instance == null) 
        {
            instance = new MatrixGenerator();
        }
        return instance;
    }
    public List<List<double>> generateMatrix(int n, int m)
    {
        List<List<double>> matrix=new List<List<double>>();
        for (int i=0; i<n; i++)
        {
            matrix.Add(new List<double>());
            for (int j=0; j<m; j++)
                matrix[i].Add(generator.NextDouble());
        }
        return matrix;
    }
}