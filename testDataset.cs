using System;
using System.Collections.Generic;
class TestDataset {
    static void printStringMatrix(IList<List<String>> matrix) {
        Console.Write("{");
        for (int i=0; i<matrix.Count; i++) {
            Console.Write("{");
            for (int j=0; j<matrix[i].Count; j++)
                if (j<matrix[i].Count-1) //Don't put the comma after the last element in the array.
                    Console.Write("\""+matrix[i][j]+"\", ");
                else
                    Console.Write("\""+matrix[i][j]+"\"");
            if (i<matrix.Count-1)
                Console.WriteLine("},");
            else
                Console.Write("}");
        }
        Console.WriteLine("};");
    }
    public static void Main() {
        Dataset firstDataset=new Dataset("example.csv");
        Dataset secondDataset=(Dataset)firstDataset.Clone();
        Console.WriteLine("Before the clearing of the first dataset:");
        printStringMatrix(firstDataset.GetData());
        printStringMatrix(secondDataset.GetData());
        firstDataset.ClearData();
        Console.WriteLine("After the clearing:");
        printStringMatrix(firstDataset.GetData());
        printStringMatrix(secondDataset.GetData());
    }
}