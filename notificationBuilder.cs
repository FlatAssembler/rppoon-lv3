using System;
class NotificationBuilder : IBuilder
{
    private string privateAuthor="Anonymous";
    private string privateTitle="Unnamed";
    private DateTime privateTime=DateTime.Now;
    private Category privateCategory=Category.INFO;
    private ConsoleColor privateColor=ConsoleColor.White;
    private string privateText="No text was set!";
    public ConsoleNotification Build()
    {
        return new ConsoleNotification(
            privateAuthor,
            privateTitle,
            privateText,
            privateTime,
            privateCategory,
            privateColor);
    }
    public IBuilder SetAuthor(String author)
    {
        privateAuthor=author;
        return this;
    }
    public IBuilder SetTitle(String title)
    {
        privateTitle=title;
        return this;
    }
    public IBuilder SetTime(DateTime time)
    {
        time=privateTime;
        return this;
    }
    public IBuilder SetLevel(Category level)
    {
        privateCategory=level;
        return this;
    }
    public IBuilder SetColor(ConsoleColor color)
    {
        privateColor=color;
        return this;
    }
    public IBuilder SetText(String text)
    {
        privateText=text;
        return this;
    }
}