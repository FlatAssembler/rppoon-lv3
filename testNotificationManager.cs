using System;
class TestNotificationManager {
    public static void Main() {
        ConsoleNotification notification=new ConsoleNotification(
            "Teo Samarzija",
            "Test Notification",
            "Hello world!",
            DateTime.Now,
            Category.INFO,
            ConsoleColor.Green
        );
        NotificationManager notificationManager=new NotificationManager();
        notificationManager.Display(notification);
    }
}