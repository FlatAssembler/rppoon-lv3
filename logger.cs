class Logger {
    private string privateFileName;
    public string fileName {
    get
    {
        return privateFileName;
    }
    set
    {
        System.IO.FileInfo fileInfo=new System.IO.FileInfo(value);
        if (!fileInfo.Exists) System.IO.File.Create(value);
        fileInfo.Refresh();
        if (fileInfo.IsReadOnly) throw new System.IO.FileNotFoundException("Can't log into a read-only file.");
        privateFileName=value;
    }}
    private Logger() {
        fileName="log.txt";
    }
    private static Logger instance;
    public static Logger GetInstance(){
        if (instance==null) instance=new Logger();
        return instance;
    }
    public void Log(string message){
        System.IO.StreamWriter file=new System.IO.StreamWriter(fileName);
        file.WriteLine(message);
        file.Dispose();
    }
}