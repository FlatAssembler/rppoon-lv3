class TestLogger {
    public static void Main() {
        Logger firstReference,secondReference;
        firstReference=Logger.GetInstance();
        secondReference=Logger.GetInstance();
        firstReference.Log("Logged using the first reference.");
        secondReference.Log("Logged using the second reference.");
        secondReference.fileName="changedFileName.txt";
        firstReference.Log("Logged using the first reference.");
        secondReference.Log("Logged using the second reference.");
        /*Now both files contain the following content:
            Logged using the first reference.
            Logging using the second reference.
        */
    }
}