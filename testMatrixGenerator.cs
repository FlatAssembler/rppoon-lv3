using System;
using System.Collections.Generic;
class TestMatrixGenerator {
    public static void Main() {
        Console.WriteLine("A random 2x3 matrix:");
        MatrixGenerator matrixGenerator=MatrixGenerator.GetInstance();
        List<List<double>> result=matrixGenerator.generateMatrix(2,3);
        for (int i=0; i<result.Count; i++) {
            for (int j=0; j<result[i].Count; j++)
                Console.Write(String.Format("{0:0.00}\t",result[i][j]));
            Console.WriteLine("");
        }
    }
}