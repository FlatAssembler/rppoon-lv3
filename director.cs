using System;
class DirectorOfNotifications {
    private IBuilder builder;
    public DirectorOfNotifications(IBuilder newBuilder) {
        builder=newBuilder;
    }
    private DirectorOfNotifications() {
        //Do nothing if this constructor is invoked, and don't allow this constructor to be invoked from outside.
    }
    public ConsoleNotification getDefaultInfo(string author) {
        builder.SetAuthor(author);
        builder.SetLevel(Category.INFO);
        builder.SetColor(ConsoleColor.Green);
        builder.SetText("This is an info built using a director.");
        builder.SetTitle("Random Info");
        return builder.Build();
    }
    public ConsoleNotification getDefaultAlert(string author) {
        builder.SetAuthor(author);
        builder.SetLevel(Category.ALERT);
        builder.SetColor(ConsoleColor.Yellow);
        builder.SetText("This is an alert built using a director.");
        builder.SetTitle("Random Alert");
        return builder.Build();
    }
    public ConsoleNotification getDefaultError(string author) {
        builder.SetAuthor(author);
        builder.SetLevel(Category.ERROR);
        builder.SetColor(ConsoleColor.Red);
        builder.SetText("This is an error message built using a director.");
        builder.SetTitle("Random Error Message");
        return builder.Build();
    }
}