using System;
class TestDirector {
    public static void Main() {
        NotificationBuilder builder=new NotificationBuilder();
        DirectorOfNotifications director=new DirectorOfNotifications(builder);
        NotificationManager manager=new NotificationManager();
        manager.Display(director.getDefaultInfo("Teo Samarzija"));
        manager.Display(director.getDefaultAlert("Teo Samarzija"));
        manager.Display(director.getDefaultError("Teo Samarzija"));
    }
}